<?php

/**
 * Parses a given file as a CSV file.
 */
class FeedsTwitterParser extends FeedsParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $raw = $fetcher_result->getRaw();
    $raw_items = !empty($raw) ? json_decode($raw) : array();
    $items = array();

    if (is_array($raw_items) && count($raw_items)) {
      foreach ($raw_items as $raw_item) {
        $item_date = date_create($raw_item->created_at);
        $item_time = $item_date->format('U');
        $item = array(
          'text' => $raw_item->text,
          'author_name' => $raw_item->user->name,
          'author_username' => $raw_item->user->screen_name,
          'author_image' => $raw_item->user->profile_image_url,
          'timestamp' => $item_time,
          'url' => url('http://twitter.com/' . $raw_item->user->screen_name . '/status/' . $raw_item->id_str),
          'guid' => $raw_item->id_str,
        );
        $items[] = $item;
      }
    }
    else {
      watchdog('feeds_twitter', 'No Twitter data received for Feeds parser from URL: @url', array('@url' => $fetcher_result->url), WATCHDOG_WARNING);
      drupal_set_message(t('No Twitter data received for Feeds parser from URL: @url', array('@url' => $fetcher_result->url)), 'error');
    }

    $query_screen_name = !empty($source->config['FeedsTwitterFetcher']['query_screen_name']) ? $source->config['FeedsTwitterFetcher']['query_screen_name'] : '';
    $result = new FeedsParserResult();
    $result->title = t('@@user-name', array('@user-name' => $query_screen_name));
    $result->description = t('The Twitter feed for @@user-name.', array('@user-name' => $query_screen_name));
    $result->link = url('http://twitter.com/' . $query_screen_name);
    $result->items = $items;
    return $result;
  }

  /**
   * Return mapping sources.
   */
  public function getMappingSources() {
    return array(
      'text' => array(
        'name' => t('Tweet text'),
        'description' => t('Text of the tweet.'),
      ),
      'author_name' => array(
        'name' => t('Author\'s display name'),
        'description' => t('Full display name of the feed item\'s author.'),
      ),
      'author_username' => array(
        'name' => t('Author\'s user name'),
        'description' => t('Twitter username of the feed item\'s author.'),
      ),
      'author_image' => array(
        'name' => t('Author image'),
        'description' => t('URL of the author\'s profile image.'),
      ),
      'timestamp' => array(
        'name' => t('Published date'),
        'description' => t('Published date as UNIX time GMT of the feed item.'),
      ),
      'url' => array(
        'name' => t('Item URL (link)'),
        'description' => t('URL of the feed item.'),
      ),
      'guid' => array(
        'name' => t('Item GUID'),
        'description' => t('Global Unique Identifier of the feed item.'),
      ),
     ) + parent::getMappingSources();
  }

}
