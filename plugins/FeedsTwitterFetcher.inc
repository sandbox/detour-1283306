<?php

/**
 * @file
 * FeedsTwitterFetcher class.
 */

/**
 * Extends the FeedsHTTPFetcher class for Twitter-specific configuration.
 */
class FeedsTwitterFetcher extends FeedsFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $url = feeds_twitter_build_url($source_config);
    return new FeedsHTTPFetcherResult($url);
  }

  /**
   * Clear caches.
   */
  public function clear(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $url = feeds_twitter_build_url($source_config);
    feeds_include_library('http_request.inc', 'http_request');
    http_request_clear_cache($url);
  }

  /**
   * Build form for Feeds importer settings page.
   */
  public function configForm(&$form_state) {
    $form = array();
    $form['info'] = array(
      '#type' => 'markup',
      '#markup' => t('There are no settings for the Twitter fetcher component.'),
    );
    return $form;
  }

  /**
   * Build form for the importer (standalone or attached to node).
   */
  public function sourceForm($source_config) {
    $form = array();
    $form['query_screen_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Twitter user name'),
      '#description' => t('Enter the Twitter user name of the Twitter feed to import.'),
      '#default_value' => isset($source_config['query_screen_name']) ? $source_config['query_screen_name'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    return $form;
  }

}
