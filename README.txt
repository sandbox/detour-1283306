
Feeds Twitter
=============

For Drupal 7. Requires Feeds module (tested with 2.0-alpha4).

This module provides a fetcher and parser plugin for the Feeds module to allow
importing tweets from Twitter's REST API.

Currently, this module only supports the statuses/user_timeline command, which
returns the most recent tweets for a given user.


Getting Started
---------------

To set up a Feeds importer to regularly import the tweets from a user as nodes,
do the following:

1. Enable this module.

2. Prepare a content type to be used for the imported tweets. Create fields (as
desired) for the components of the tweet, such as: tweet text, URL, author's
user name, author's display name, author's image URL, etc.

3. Create a new Feeds importer from the "Feeds importers" admin page at:
admin/structure/feeds

4. Under "Basic settings", select your desired options for whether the importer
is attached to its own content type or using the standalone form, how often the
import should occur, etc.

5. Change the Fetcher to Twitter Fetcher.

6. Change the Parser to Twitter Parser.

7. Change the Processor to Node Processor.

8. In Settings for Node Processor, choose whether to keep, replace, or update
nodes related to the imported tweets, and choose the content type in to which to
import the tweet.

9. In Mappings for Node Processor, assign the fields in the content type that
the source fields from the tweet should populate.

10. Run the importer from the standalone form or node form (as configured).


Development
-----------

This module could be extended to allow using more Twitter API commands, such as
statuses/home_timeline (the tweets that a logged-in user sees as their home),
or search (allows filtering with complex criteria).

If you're interested in helping to implement these features or co-maintain this
as a contributed module, please contact me.
